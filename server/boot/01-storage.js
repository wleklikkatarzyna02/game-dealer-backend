const containersNames = ["attachments"];
module.exports = (app) => {
  app.models.container.getContainers((err, containers) => {
    const availableContainers = containers.map((container) => container.name);
    containersNames.forEach((containerName) => {
      if (availableContainers.includes(containerName)) return;
      app.models.container.createContainer({
        name: containerName,
      });
    });
  });
};
