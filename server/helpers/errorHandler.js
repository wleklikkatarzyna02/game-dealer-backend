class CustomError extends Error {
  constructor(statusCode, code, message, details) {
    super(message);
    Object.assign(this, { statusCode, code, details });
  }
}

module.exports = {
  CustomError,
  AUTH_ERR: new CustomError(403, "Forbidden", "you don't have this permission"),
  SERVER_ERR: new CustomError(
    500,
    "Internal Server Error",
    "Error in Server while handling Request"
  ),
  UNAUTHORIZED_ERR: new CustomError(
    401,
    "Unauthorized",
    "You are not allowed to perform this action"
  ),
};
