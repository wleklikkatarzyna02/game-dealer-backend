const compressImages = require("compress-images");

module.exports = {
  async compress() {
    const inputPath =
      "server/containers/attachments/*.{jpg,JPG,jpeg,JPEG,png,svg,gif}";
    const outputPath = "server/containers/attachments/";
    const options = {
      compress_force: false,
      statistic: true,
      autoupdate: true,
    };

    const engines = [
      { jpg: { engine: "webp", command: false } },
      { png: { engine: "webp", command: false } },
      { svg: { engine: "webp", command: false } },
      { gif: { engine: "webp", command: false } },
    ];
    return compressImages(
      inputPath,
      outputPath,
      options,
      false,
      ...engines,
      (error, completed, statistic) => {
        console.log("-------------");
        console.log(error);
        console.log(completed);
        console.log(statistic);
        console.log("-------------");
      }
    );
  },
};
