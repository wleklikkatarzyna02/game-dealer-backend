module.exports = {
  order_id: 2321160662,
  amount_cents: "1000000",
  items: [],
  billing_data: {
    apartment: "3",
    email: "sherif_nasrat@hotmail.com",
    floor: "2",
    first_name: "Sherif",
    last_name: "Nasrat",
    street: "Ethan Land",
    building: "951",
    postal_code: "19962",
    city: "Cairo",
    phone_number: "01062264173",
    state: "Cairo",
    country: "Egypt",
  },
};
