const moment = require("moment");

module.exports = (Model) => {
  Model.defineProperty("createdAt", {
    type: "string",
    readOnly: true,
    visible: true,
    index: true,
  });

  Model.observe("before save", async (ctx) => {
    if (ctx.data && ctx.data.createdAt) delete ctx.data.createdAt;
    if (ctx.instance && ctx.isNewInstance) {
      ctx.instance.createdAt = moment()
        .utcOffset("+02:00")
        .format("YYYY-MM-DDTHH:mm:ss.SSS[Z]");
    }
  });
};
