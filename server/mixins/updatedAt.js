const moment = require("moment");

module.exports = (Model, options) => {
  Model.defineProperty("updatedAt", {
    type: "string",
    readOnly: true,
    visible: true,
    index: true,
  });

  Model.observe("before save", async (ctx) => {
    if (ctx.isNewInstance) {
      ctx.instance.updatedAt = moment()
        .utcOffset("+02:00")
        .format("YYYY-MM-DDTHH:mm:ss.SSS[Z]");
      return;
    }
    ctx.data = ctx.data || {};
    ctx.data.updatedAt = moment()
      .utcOffset("+02:00")
      .format("YYYY-MM-DDTHH:mm:ss.SSS[Z]");
    if (ctx.instance)
      ctx.instance.updatedAt = moment()
        .utcOffset("+02:00")
        .format("YYYY-MM-DDTHH:mm:ss.SSS[Z]");
  });
};
