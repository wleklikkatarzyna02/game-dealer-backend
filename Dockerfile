FROM node:12-stretch-slim

LABEL maintainer="Hossam Mohamed <eng.hossam4mohamed@gmail.com>"

RUN mkdir -p /app

WORKDIR /app

COPY package.json package-lock.json ./

RUN npm install && npm audit fix

COPY --chown=node:node ./ ./

USER node

CMD node .